package br.edu.ufam.mbg1icomp.perfil.model;

/**
 * Created by daniel on 12/8/15.
 */
public class Usuario {
    private transient int codigo;
    private String nome = "";
    private int sexo = 0;
    private String foto;
    private transient String link_facebook;
    private transient String link_google_plus;
    private String email;
    private String senha;

    public Usuario(String email, String senha){
        this.email = email;
        this.senha = senha;
    }

    public Usuario(String nome, String email, String senha, int sexo){

        this.nome = nome;
        this.email = email;
        this.senha = senha;
        this.sexo = sexo;

    }
}
