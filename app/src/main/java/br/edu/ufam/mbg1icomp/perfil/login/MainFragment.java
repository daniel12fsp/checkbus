package br.edu.ufam.mbg1icomp.perfil.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


import com.github.gorbin.asne.core.SocialNetwork;
import com.github.gorbin.asne.core.SocialNetworkManager;
import com.github.gorbin.asne.core.listener.OnLoginCompleteListener;
import com.github.gorbin.asne.facebook.FacebookSocialNetwork;
import com.github.gorbin.asne.googleplus.GooglePlusSocialNetwork;
import com.google.gson.Gson;
import com.loopj.android.http.BaseJsonHttpResponseHandler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import br.edu.ufam.mbg1icomp.perfil.R;
import br.edu.ufam.mbg1icomp.perfil.main.MainActivity;
import br.edu.ufam.mbg1icomp.perfil.model.Usuario;
import br.edu.ufam.mbg1icomp.perfil.web.Servidor;
import br.edu.ufam.mbg1icomp.perfil.web.Utils;
import cz.msebera.android.httpclient.Header;

public class MainFragment extends Fragment implements SocialNetworkManager.OnInitializationCompleteListener, OnLoginCompleteListener {
    public static SocialNetworkManager mSocialNetworkManager;

    private Button facebook;
    private Button googleplus;
    private View rootView;

    public MainFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.main_fragment, container, false);
        //((LoginActivity)getActivity()).getSupportActionBar().setTitle(R.string.app_name);
        Button btnEntrar = (Button) rootView.findViewById(R.id.btnEntrarLogin);
        btnEntrar.setOnClickListener(entrar);

        // init buttons and set Listener
        facebook = (Button) rootView.findViewById(R.id.facebook);
        facebook.setOnClickListener(loginClick);
        googleplus = (Button) rootView.findViewById(R.id.googleplus);
        googleplus.setOnClickListener(loginClick);


        //Chose permissions
        ArrayList<String> fbScope = new ArrayList<String>();
        fbScope.addAll(Arrays.asList("public_profile, email, user_friends"));

        //Use manager to manage SocialNetworks
        mSocialNetworkManager = (SocialNetworkManager) getFragmentManager().findFragmentByTag(LoginActivity.SOCIAL_NETWORK_TAG);

        //Check if manager exist
        if (mSocialNetworkManager == null) {
            mSocialNetworkManager = new SocialNetworkManager();

            //Init and add to manager FacebookSocialNetwork
            FacebookSocialNetwork fbNetwork = new FacebookSocialNetwork(this, fbScope);
            mSocialNetworkManager.addSocialNetwork(fbNetwork);


            //Init and add to manager GooglePlusSocialNetwork
            //GooglePlusSocialNetwork gpNetwork = new GooglePlusSocialNetwork(this);
            //mSocialNetworkManager.addSocialNetwork(gpNetwork);

            //Initiate every network from mSocialNetworkManager
            getFragmentManager().beginTransaction().add(mSocialNetworkManager, LoginActivity.SOCIAL_NETWORK_TAG).commit();
            mSocialNetworkManager.setOnInitializationCompleteListener(this);
        } else {
            //if manager exist - get and setup login only for initialized SocialNetworks
            if(!mSocialNetworkManager.getInitializedSocialNetworks().isEmpty()) {
                List<SocialNetwork> socialNetworks = mSocialNetworkManager.getInitializedSocialNetworks();
                for (SocialNetwork socialNetwork : socialNetworks) {
                    socialNetwork.setOnLoginCompleteListener(this);
                    initSocialNetwork(socialNetwork);
                }
            }
        }


        return rootView;
    }

    private void initSocialNetwork(SocialNetwork socialNetwork){
        if(socialNetwork.isConnected()){
            switch (socialNetwork.getID()){
                case FacebookSocialNetwork.ID:
                    facebook.setText("Show Facebook profile");
                    break;
                case GooglePlusSocialNetwork.ID:
                    googleplus.setText("Show GooglePlus profile");
                    break;
            }
        }


    }
    @Override
    public void onSocialNetworkManagerInitialized() {
        //when init SocialNetworks - get and setup login only for initialized SocialNetworks
        for (SocialNetwork socialNetwork : mSocialNetworkManager.getInitializedSocialNetworks()) {
            socialNetwork.setOnLoginCompleteListener(this);
            initSocialNetwork(socialNetwork);
        }
    }


            private View.OnClickListener entrar = new View.OnClickListener() {


                @Override
                public void onClick(View v) {
                    EditText editEmail = (EditText) rootView.findViewById(R.id.editEmailLogin);
                    final EditText editSenha = (EditText) rootView.findViewById(R.id.editSenhaLogin);
                    String str = "Falta preencher";
                    if(editEmail.getText().toString().equals("")){
                        str += "Email";

                    } else if(editSenha.getText().toString().equals("")){
                        str += "e Senha";
                    }
                    if(str.equals("Falta preencher")){
                        String url = Utils.ip_servidor + "onibus/app.php/usuario/login/proprio";
                        Servidor servidor = new Servidor();
                        Usuario usuario = new Usuario(editEmail.getText().toString(),
                                editSenha.getText().toString());
                        servidor.prepareParametre(usuario);
                        servidor.post(url, new BaseJsonHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, String rawJsonResponse, Object response) {
                                Intent intent = new Intent(rootView.getContext(), MainActivity.class);
                                rootView.getContext().startActivity(intent);
                                MainFragment.this.getActivity().finish();

                            }

                            @Override
                            public void onFailure(int statusCode, Header[] headers, Throwable throwable, String rawJsonData, Object errorResponse) {
                                Utils.mensagem(getContext(), "Sem conexão");
                                editSenha.setText("");

                            }

                            @Override
                            protected Object parseResponse(String rawJsonData, boolean isFailure) throws Throwable {
                                Gson gson = new Gson();
                                Object object = gson.fromJson(rawJsonData, Usuario.class);
                                return object;
                            }
                        });

                    }else{
                        Utils.mensagem(getContext(), str);
                    }
                }
            };


            //Login listener

    private View.OnClickListener loginClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            int networkId = 0;
            switch (view.getId()){
                case R.id.facebook:
                    networkId = FacebookSocialNetwork.ID;
                    break;
                case R.id.googleplus:
                    networkId = GooglePlusSocialNetwork.ID;
                    break;
            }
            SocialNetwork socialNetwork = mSocialNetworkManager.getSocialNetwork(networkId);
            if(!socialNetwork.isConnected()) {
                if(networkId != 0) {
                    socialNetwork.requestLogin();
                    LoginActivity.showProgress("Loading social person");
                } else {
                    Toast.makeText(getActivity(), "Wrong networkId", Toast.LENGTH_LONG).show();
                }
            } else {
                startProfile(socialNetwork.getID());
            }
        }
    };

    @Override
    public void onLoginSuccess(int networkId) {
        LoginActivity.hideProgress();
        startProfile(networkId);
    }

    @Override
    public void onError(int networkId, String requestID, String errorMessage, Object data) {
        LoginActivity.hideProgress();
        Toast.makeText(getActivity(), "ERROR: " + errorMessage, Toast.LENGTH_LONG).show();
    }

    private void startProfile(int networkId){
        ProfileFragment profile = ProfileFragment.newInstannce(networkId);
        getActivity().getSupportFragmentManager().beginTransaction()
                .addToBackStack("profile")
                .replace(R.id.container, profile)
                .commit();
    }
}
