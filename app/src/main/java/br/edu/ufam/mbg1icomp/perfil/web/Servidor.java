package br.edu.ufam.mbg1icomp.perfil.web;

import android.util.Log;


import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.BaseJsonHttpResponseHandler;
import com.loopj.android.http.RequestHandle;

import java.io.UnsupportedEncodingException;

import br.edu.ufam.mbg1icomp.perfil.model.Resposta;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.ByteArrayEntity;

/**
 * Created by daniel on 12/8/15.
 */
public class Servidor extends AsyncHttpClient {
    private ByteArrayEntity entity = null;
    private Class _class = null;

    public Servidor(){
        this._class = Resposta.class;
    }
    public Servidor(Class _class){
        this._class = _class;
    }

    public RequestHandle post(String url) {
        return super.post(null, url, this.entity, "application/json", new BaseJsonHttpResponseHandler(){


           @Override
            protected Object parseResponse(String rawJsonData, boolean isFailure) throws Throwable {
               Log.d("JsonData", rawJsonData);
               if(rawJsonData.equals("{\"result\":\"ok\"}")) {
                   throw new Exception();
               }
               return null;
               /*
               Gson gson = new Gson();
               Object object = gson.fromJson(rawJsonData, Servidor.this._class);
               Log.d("json", object.toString());
               return object;*/
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String rawJsonResponse, Object response) {
                //TODO implementar msg em caso de sucesso

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, String rawJsonData, Object errorResponse) {
                //TODO implementar msg em caso de falha

            }


        });
    }

    public RequestHandle post(String url, BaseJsonHttpResponseHandler basejson) {
        return super.post(null, url, this.entity, "application/json", basejson);
    }

    public void prepareParametre(Object object){
        Gson gson = new Gson();
        String json = gson.toJson(object);
        Log.d("json", json);

        //--
        try {
            this.entity = new ByteArrayEntity(json.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
}
