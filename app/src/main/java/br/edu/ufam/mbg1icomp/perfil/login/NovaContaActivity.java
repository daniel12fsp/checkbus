package br.edu.ufam.mbg1icomp.perfil.login;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import br.edu.ufam.mbg1icomp.perfil.R;
import br.edu.ufam.mbg1icomp.perfil.model.Usuario;
import br.edu.ufam.mbg1icomp.perfil.web.Servidor;
import br.edu.ufam.mbg1icomp.perfil.web.Utils;


public class NovaContaActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nova_conta);
        Button btnSalvar = (Button) findViewById(R.id.btnSalvarNovaConta);
        btnSalvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                EditText editNome = (EditText)findViewById(R.id.editNomeNovaConta);
                EditText editEmail = (EditText)findViewById(R.id.editEmailNovaConta);
                EditText editSenha = (EditText)findViewById(R.id.editSenhaNovaConta);
                EditText editRepeticaoSenha = (EditText)findViewById(R.id.editRepeticaoSenhaNovaConta);
                RadioGroup radioSexo = (RadioGroup)findViewById(R.id.radioSexo);
                String radiovalue = ((RadioButton)findViewById(radioSexo.getCheckedRadioButtonId())).getText().toString();
                Log.d("psw", editSenha.getText().toString());
                Log.d("repsw", editRepeticaoSenha.getText().toString());
                if(editSenha.getText().toString().equals(editRepeticaoSenha.getText().toString())) {

                    String url = Utils.ip_servidor + "/onibus/app.php/usuario";
                    Servidor servidor = new Servidor();
                    Usuario usuario = new Usuario(editNome.getText().toString(), editEmail.getText().toString(),
                            editSenha.getText().toString(), 0);
                    servidor.prepareParametre(usuario);
                    servidor.post(url);
                }else{

                    editRepeticaoSenha.setText("");
                    Utils.mensagem(getApplicationContext(), "Senha diferentes!");

                }
            }
        });

        Button btnCancelar = (Button) findViewById(R.id.btnCancelarNovaConta);
        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NovaContaActivity.this.finish();
            }
        });

    }
}


